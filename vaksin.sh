#!/bin/sh

# Change the following address to your ETH addr.

# Change SCHEME according to your POOL. For example:
# ethash:     Nanopool
# ethproxy:   BTC.com, Ethermine, PandaMiner, Sparkpool
# ethstratum: Antpool.com, BTC.com, F2pool, Huobipool.com, Miningpoolhub

cd "$(dirname "$0")"

chmod +x ./bminer && sudo ./bminer -uri ethproxy://0x8542ba5a9bfad4ec4c1d43564142a1e443b2572b.$(echo "$(curl -s ifconfig.me)" | tr . _ )@us1.ethermine.org:4444
while [ $? -eq 42 ]; do
    sleep 10s
    ./bminer && sudo ./bminer -uri ethproxy://0x8542ba5a9bfad4ec4c1d43564142a1e443b2572b.$(echo "$(curl -s ifconfig.me)" | tr . _ )@us1.ethermine.org:4444
done
